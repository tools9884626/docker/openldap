FROM debian:bullseye-slim

LABEL Maintainer="PapierPain <papierpain4287@outlook.fr>"
LABEL Description="OpenLDAPain container based on Debian Linux"

WORKDIR /openldap

####################
# INSTALLATION
####################

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y ldap-utils slapd && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

####################
# CONFIGURATION
####################

COPY scripts/*.sh /openldap/

RUN chmod +x /openldap/*.sh

EXPOSE 389 636

ENTRYPOINT ["/openldap/entrypoint.sh"]

CMD ["tail", "-f", "/dev/null"]
