# OpenLDAP image

## Description

Image docker (debian) avec OpenLDAP.

## Usage

Créer un fichier `docker-compose.yml` :

```yml
version: "3"

services:
  ldap:
    image: registry.papierpain.fr/docker/openldap/test:3-ca-marche-toujours-paaaaaas
    environment:
      - LDAP_ROOT=dc=chocolat,dc=fr
      - LDAP_ADMIN_USERNAME=admin
      - LDAP_ADMIN_PASSWORD=Password
    volumes:
      - ./data.ldif:/openldap/ldifs/data.ldif
```

Créer un fichier `data.ldif` :

```ldif
dn: dc=chocolat,dc=fr
objectClass: top
objectClass: dcObject
objectClass: organization
o: chocolat.fr
dc: chocolat

dn: cn=admin,dc=chocolat,dc=fr
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
userPassword: {SSHA}gc6ufN47jtrzOOEMeOwMuxPCAjsmLWfL

dn: ou=users,dc=chocolat,dc=fr
objectClass: organizationalUnit
ou: users

dn: uid=Banane,ou=users,dc=chocolat,dc=fr
objectClass: top
objectClass: person
objectClass: inetOrgPerson
cn: Boum
sn: Banane
mail: Banane@chocolat.fr
uid: Banane
userPassword: {SSHA}Zdc2W1ENsvqZDUxxBpRdy62OXu/+HIZv

dn: uid=Patate,ou=users,dc=chocolat,dc=fr
objectClass: top
objectClass: person
objectClass: inetOrgPerson
cn: Boum
sn: Patate
mail: Patate@chocolat.fr
uid: Patate
userPassword: {SSHA}ZgMeH8V70fu40GkGs0z3bsAI3jt77OST
```
