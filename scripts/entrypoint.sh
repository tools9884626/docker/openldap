#!/bin/bash
set -e

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#! MAIN:
#!   LDAP initialization.
#!
#! USAGE:
#!   ./entrypoint.sh
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Set default values
LDAP_ROOT="${LDAP_ROOT:-dc=example,dc=fr}"
LDAP_ADMIN_USERNAME="${LDAP_ADMIN_USERNAME:-admin}"
LDAP_ADMIN_PASSWORD="${LDAP_ADMIN_PASSWORD:-root}"
LDAP_CUSTOM_DB_DIR="${LDAP_CUSTOM_DB_DIR:-/var/lib/ldap}"
LDAP_ADMIN_DN="cn=${LDAP_ADMIN_USERNAME},${LDAP_ROOT}"

# If the database already exists, do nothing
if [ -f /etc/ldap/slapd.d/cn\=config/olcDatabase\=\{1\}mdb.ldif ] && [ ! -z $(grep -q "${LDAP_ROOT}" /etc/ldap/slapd.d/cn\=config/olcDatabase\=\{1\}mdb.ldif && echo $?) ]; then
    echo "Database already exists, skipping default initialization."

    # Start LDAP server
    slapd -F /etc/ldap/slapd.d -h "ldap:/// ldaps:/// ldapi:///"
else
    echo "Database initialization"

    ./setup.sh ${LDAP_CUSTOM_DB_DIR} ${LDAP_ADMIN_DN} ${LDAP_ADMIN_PASSWORD} ${LDAP_ROOT}

    # Start LDAP server
    slapd -F /etc/ldap/slapd.d -h "ldap:/// ldaps:/// ldapi:///"

    # Add ldif files to the database if exist in the folder /openldap/ldifs
    if [ -n "$(ls -A /openldap/ldifs/*.ldif 2>/dev/null)" ]; then
        LDIF_FILES=$(ls -A /openldap/ldifs/*.ldif)

        for LDIF_FILE in $LDIF_FILES; do
            ldapadd -x -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -f $LDIF_FILE
            echo "$LDIF_FILE added to the database"
        done
    fi
fi

echo ""
exec "$@"
